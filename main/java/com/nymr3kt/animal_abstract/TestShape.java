/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nymr3kt.animal_abstract;

/**
 *
 * @author nymr3kt
 */
public class TestShape {

    public static void main(String[] args) {
        Circle c1 = new Circle(1.5);
        Circle c2 = new Circle(4.5);
        Circle c3 = new Circle(5.5);
        System.out.println(c1);
        System.out.println(c2);
        System.out.println(c3);
        System.out.println("---------------------------------------------------------------------------------------------------");

        Squre s1 = new Squre(2.5);
        Squre s2 = new Squre(1.5);
        Squre s3 = new Squre(5.2);
        System.out.println(s1);
        System.out.println(s2);
        System.out.println(s3);
        System.out.println("---------------------------------------------------------------------------------------------------");

        Rectangle rec1 = new Rectangle(1.2, 2.1);
        Rectangle rec2 = new Rectangle(2.3, 3.5);
        Rectangle rec3 = new Rectangle(0.5, 5.2);
        System.out.println(rec1);
        System.out.println(rec2);
        System.out.println(rec3);
        System.out.println("---------------------------------------------------------------------------------------------------");

        Shape[] shape = {c1, c2, c3, s1, s2, s3, rec1, rec2, rec3};
        for (int i = 0; i < shape.length; i++) {
            System.out.printf(shape[i].getName() + " Area :" + "%.2f", shape[i].calArea());
            System.out.println();
        }

    }
}
