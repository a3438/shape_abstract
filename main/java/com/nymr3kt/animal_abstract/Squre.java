/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nymr3kt.animal_abstract;

/**
 *
 * @author nymr3kt
 */
public class Squre extends Shape {

    private double side;

    public Squre(double side) {
        super("Squre");
        this.side = side;
    }

    public double getSide() {
        return side;
    }

    public void setSide(double side) {
        this.side = side;
    }

    @Override
    public double calArea() {
        return side * side;
    }

    @Override
    public String toString() {
        return "Squre[" + "side = " + side + ']';
    }

}
