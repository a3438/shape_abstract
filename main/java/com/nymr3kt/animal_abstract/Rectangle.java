/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nymr3kt.animal_abstract;

/**
 *
 * @author nymr3kt
 */
public class Rectangle extends Shape{
    private double w ;
    private double s ;

    public Rectangle(double w, double s) {
        super("Rectangle");
        this.w = w;
        this.s = s;
    }

    public double getW() {
        return w;
    }

    public double getS() {
        return s;
    }

    public void setW(double w) {
        this.w = w;
    }

    public void setS(double s) {
        this.s = s;
    }
    
    
    @Override
    public double calArea() {
        return s * w ;
    }

    @Override
    public String toString() {
        return "Rectangle[" + "w = " + w + ", s = " + s + ']';
    }
   
}
